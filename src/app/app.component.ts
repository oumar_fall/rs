import { Component } from '@angular/core';
import * as jwt_decode from "jwt-decode";
import { TokenService } from './token.service';
import { AuthService } from './auth.service';
import { Route, Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  constructor(private tokenService: TokenService, private authService: AuthService, private router: Router){
    const myToken = this.authService.getToken();
    console.log(myToken);
    if(myToken != null ){
      const decode = jwt_decode(myToken);
    console.log("+++++++",decode);
    this.authService.getMemberById(decode.subject).subscribe(
      data => console.log("User datas ",data)
    )

    } else {
      this.router.navigate(['/login']);
    }
  }

}
